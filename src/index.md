Title: Qu'est-ce que My Workshop ?

# Qu'est-ce que _My Workshop_ ?

## Le contexte
Le principe des [fab labs](https://fr.wikipedia.org/wiki/Fab_lab) repose sur le partage libre d'espaces, de machines, de compétences et de savoirs. Les fab labs, [makerspaces](https://fr.wikipedia.org/wiki/Makerspace) ou autres tiers lieux sont souvent confrontés aux mêmes difficultés en ce qui concerne la documentation des projets open sources hardware développés par leurs membres.

### Intégration de la documentation dès le début du prototypage
Malgré une forte motivation des _makers_ à vouloir documenter leurs projets, la plupart du temps ils se heurtent à la complexité des outils numériques mis à disposition et à leur hétérogénéité (wikis, plateformes en ligne, repo GitHub, etc.).

De plus, la documentation intervient la plupart du temps à la fin du processus de prototypage, à un moment ou l'on s’aperçoit qu'on a oublié de prendre une photo, de noter un lien qui nous a été utile, et le processus de documentation se complique !

L'idée de __My Workshop__ est de fournir une interface simple et conviviale de prises de notes (le [cahier de labo](concepts/labbook)) qui accompagne le maker lors du prototypage. Lorsqu'arrive le moment de la rédaction d'une documentation de projet ou d'un tutoriel, il n'y a plus qu'à aller piocher dans ses notes, les mettre en forme et partager ses réalisations au monde entier !

### Mutualiser les ressources
Rien qu'à Rennes et alentours, il existe plus d'une dizaine de fab labs, makerspaces, espaces numériques de maison de quartier et autres tiers lieux. Ils sont associatifs, universitaires ou privés et souvent membres du réseau [LabFab](http://www.labfab.fr/).

En fréquentant ces lieux, on constate qu'ils disposent bien souvent des mêmes machines, des mêmes outils et on y rencontre les mêmes _makers_ ! Chacun de ces lieux, lorsqu'il en a le temps ou les ressources, tente de mettre en place sa propre documentation d'utilisation du matériel notamment.

L'une des fonctionnalités de __My Workshop__ sera de permettre à chacune de ces structures de partager ses notices d'utilisation de machines et outils, ses tutoriels, ses bonnes pratiques de design, de prototypage et de fabrication pour élaborer un socle commun de connaissances partagées et à jour.

### Faciliter la gestion d'un fab lab
Un autre sujet récurent dans les tiers lieux concerne la gestion même du lieu, de ses machines, des créneaux disponibles, des événements tels que les hackathons, les formations, etc. __My Workshop__ a pour ambition de simplifier cette gestion en fournissant un agenda, un module de réservations de salles ou de machines et des espaces de documentation temporaires et partagés le temps d'un workshop.

### Collaborer sur des projets d'envergure
Ce qui est intéressant dans les fab labs, c'est aussi de se retrouver autour de projets communs. Pour faciliter les échanges, l'intégration de nouveaux participants ou la documentation, __My Workshop__ proposera des espaces communs (les [forges de projet](concepts/forge)) pour gérer ces projets complexes faisant intervenir différents _makers_ dans différents lieux.

## Notre vision

### Simple, mais pas simpliste !
Pour que cet outil soit efficace, ludique et que les _makers_ s'en emparent, nous pensons qu'il doit être simple. Simple dans son utilisation, simple dans son [déploiement](deployment) et sa maintenance.

## La feuille de route
__My Workshop__ est un projet qui en est à ses prémices. L'équipe de développeurs bénévoles est en train de structurer le projet pour intégrer facilement de nouveaux développeurs et sortir une version de démo convaincante ...

_Stay tuned!_
